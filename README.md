CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------

Technocrat companion is a helper module for Technocrat Starterkit theme.

 * Provides a command to generate a subtheme with Drush:
   `drush generate theme-technocrat`

This will also download the Technocrat Starterkit theme.

Have look at [Composer template for Drupal projects]
(https://github.com/drupal-composer/drupal-project)
if you are not familiar on how to manage Drupal projects with composer.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/technocrat_companion).

INSTALLATION
------------

 * Install the Technocrat companion module as you would normally install
   a contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

There is no configuration.
