<?php

namespace Drupal\technocrat_companion\Generators;

use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * {@inheritdoc}
 */
class SubthemeGenerator extends BaseGenerator {
  /**
   * Drush command name.
   *
   * @var string
   */
  protected $name = 'd8:theme-technocrat';
  /**
   * Drush command description.
   *
   * @var string
   */
  protected $description = 'Generates a Technocrat Starterkit subtheme.';
  /**
   * Drush alias.
   *
   * @var string
   */
  protected $alias = 'technocrat-theme';
  /**
   * The path of template.
   *
   * @var string
   */
  protected $templatePath = __DIR__;
  /**
   * Themes directory.
   *
   * @var string
   */
  protected $destination = 'themes';

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $questions['name'] = new Question('Theme name');
    $questions['name']->setValidator([Utils::class, 'validateRequired']);
    $questions['machine_name'] = new Question('Theme machine name');
    $questions['machine_name']->setValidator(
      [Utils::class, 'validateMachineName']
    );
    $questions['description'] = new Question(
          'Description',
          'Custom sub-theme, inherits from the Technocrat Starterkit theme'
      );

    $vars = &$this->collectVars($input, $output, $questions);

    $prefix = 'custom/' . $vars['machine_name'] . '/';

    $zfPath = DRUPAL_ROOT . '/' . drupal_get_path('theme', 'technocrat_starterkit') . '/';

    $patterns = ['/^name: .*$/m', '/^description: .*$/m', '/STARTER/'];
    $replacements = ['name: ' . $vars['name'],
      'description: "' . addslashes($vars['description']) . '"',
      $vars['machine_name'],
    ];
    $this->addFile()
      ->path($prefix . '{machine_name}.info.yml')
      ->content(preg_replace($patterns, $replacements, file_get_contents(
              $zfPath . 'STARTER/STARTER.info.yml.txt'
            )));

    $this->addFile()
      ->path($prefix . '{machine_name}.libraries.yml')
      ->content(str_replace('STARTER', $vars['machine_name'], file_get_contents(
              $zfPath . 'STARTER/STARTER.libraries.yml'
            )));

    $this->addFile()
      ->path($prefix . '{machine_name}.theme')
      ->content(str_replace('STARTER', $vars['machine_name'], file_get_contents(
              $zfPath . 'STARTER/STARTER.theme'
            )));

    $this->addFile()
      ->path($prefix . 'README.md')
      ->content(file_get_contents(
              $zfPath . 'STARTER/README.md'
            ));

    $this->addFile()
      ->path($prefix . '.csscomb.json')
      ->content(file_get_contents(
              $zfPath . 'STARTER/.csscomb.json'
            ));

    $this->addFile()
      ->path($prefix . '.gitignore')
      ->content(file_get_contents(
              $zfPath . 'STARTER/.gitignore'
            ));

    $this->addFile()
      ->path($prefix . 'gulpfile.js')
      ->content(file_get_contents(
              $zfPath . 'STARTER/gulpfile.js'
            ));

    $this->addFile()
      ->path($prefix . 'package.json')
      ->content(file_get_contents(
              $zfPath . 'STARTER/package.json'
            ));

    // $this->addFile()
    // ->path($prefix . 'css/{machine_name}.css')
    // ->content(str_replace('STARTER', $vars['machine_name'],
    // file_get_contents($zfPath . 'STARTER/css/STARTER.css'
    // )));
    // $gif_files = glob($zfPath . 'STARTER/images/foundation/orbit/*.gif');
    // foreach ($gif_files as $gif_file) {
    // $this->addFile()
    // ->path($prefix . 'images/foundation/orbit/' . basename($gif_file))
    // ->content(file_get_contents($gif_file));
    // }
    // $jpg_files = glob($zfPath . 'STARTER/images/foundation/orbit/*.jpg');
    // foreach ($jpg_files as $jpg_file) {
    // $this->addFile()
    // ->path($prefix . 'images/foundation/orbit/' . basename($jpg_file))
    // ->content(file_get_contents($jpg_file));
    // }
    // $png_files = glob($zfPath . 'STARTER/images/foundation/orbit/*.png');
    // foreach ($png_files as $png_file) {
    // $this->addFile()
    // ->path($prefix . 'images/foundation/orbit/' . basename($png_file))
    // ->content(file_get_contents($png_file));
    // }
    $this->addFile()
      ->path($prefix . 'js/custom.js')
      ->content(file_get_contents(
              $zfPath . 'STARTER/js/custom.js'
            ));

    $this->addFile()
      ->path($prefix . 'css/style.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/css/style.css'
      ));

    $this->addFile()
      ->path($prefix . 'scss/style.scss')
      ->content(file_get_contents(
              $zfPath . 'STARTER/scss/style.scss'
            ));

    $this->addFile()
      ->path($prefix . 'scss/_mixins.scss')
      ->content(file_get_contents(
              $zfPath . 'STARTER/scss/_mixins.scss'
            ));

    $this->addFile()
      ->path($prefix . 'scss/_overrides.scss')
      ->content(file_get_contents(
              $zfPath . 'STARTER/scss/_overrides.scss'
            ));

    $scss_base_files = glob($zfPath . 'STARTER/scss/base/*.scss');
    foreach ($scss_base_files as $scss_base_file) {
      $this->addFile()
        ->path($prefix . 'scss/base/' . basename($scss_base_file))
        ->content(file_get_contents($scss_base_file));
    }

    $scss_layout_files = glob($zfPath . 'STARTER/scss/blocks/*.scss');
    foreach ($scss_layout_files as $scss_layout_file) {
      $this->addFile()
        ->path($prefix . 'scss/blocks/' . basename($scss_layout_file))
        ->content(file_get_contents($scss_layout_file));
    }

    $scss_layout_files = glob($zfPath . 'STARTER/scss/elements/*.scss');
    foreach ($scss_layout_files as $scss_layout_file) {
      $this->addFile()
        ->path($prefix . 'scss/elements/' . basename($scss_layout_file))
        ->content(file_get_contents($scss_layout_file));
    }

    $scss_layout_files = glob($zfPath . 'STARTER/scss/layout/*.scss');
    foreach ($scss_layout_files as $scss_layout_file) {
      $this->addFile()
        ->path($prefix . 'scss/layout/' . basename($scss_layout_file))
        ->content(file_get_contents($scss_layout_file));
    }

    $scss_modules_files = glob($zfPath . 'STARTER/scss/pages/*.scss');
    foreach ($scss_modules_files as $scss_modules_file) {
      $this->addFile()
        ->path($prefix . 'scss/pages/' . basename($scss_modules_file))
        ->content(file_get_contents($scss_modules_file));
    }

    $this->addFile()
      ->path($prefix . 'fonts/Bold/OpenSans-Bold.eot')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Bold/OpenSans-Bold.eot'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Bold/OpenSans-Bold.svg')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Bold/OpenSans-Bold.svg'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Bold/OpenSans-Bold.ttf')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Bold/OpenSans-Bold.ttf'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Bold/OpenSans-Bold.woff')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Bold/OpenSans-Bold.woff'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Bold/OpenSans-Bold.woff2')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Bold/OpenSans-Bold.woff2'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Light/OpenSans-Light.eot')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Light/OpenSans-Light.eot'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Light/OpenSans-Light.svg')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Light/OpenSans-Light.svg'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Light/OpenSans-Light.ttf')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Light/OpenSans-Light.ttf'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Light/OpenSans-Light.woff')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Light/OpenSans-Light.woff'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Light/OpenSans-Light.woff2')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Light/OpenSans-Light.woff2'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Regular/OpenSans-Regular.eot')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Bold/OpenSans-Bold.eot'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Regular/OpenSans-Regular.svg')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Regular/OpenSans-Regular.svg'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Regular/OpenSans-Regular.ttf')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Regular/OpenSans-Regular.ttf'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Regular/OpenSans-Regular.woff')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Regular/OpenSans-Regular.woff'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/Regular/OpenSans-Regular.woff2')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/Regular/OpenSans-Regular.woff2'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/css/animation.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/css/animation.css'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/css/fontello.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/css/fontello.css'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/css/fontello-codes.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/css/fontello-codes.css'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/css/fontello-embedded.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/css/fontello-embedded.css'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/css/fontello-ie7.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/css/fontello-ie7.css'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/css/fontello-ie7-codes.css')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/css/fontello-ie7-codes.css'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/fonts/fontello.eot')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/font/fontello.eot'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/fonts/fontello.svg')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/font/fontello.svg'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/fonts/fontello.ttf')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/font/fontello.ttf'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/fonts/fontello.woff')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/font/fontello.woff'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/fonts/fontello.woff2')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/font/fontello.woff2'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/config.json')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/config.json'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/demo.html')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/demo.html'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/LICENSE.txt')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/LICENSE.txt'
      ));

    $this->addFile()
      ->path($prefix . 'fonts/fontello/README.txt')
      ->content(file_get_contents(
        $zfPath . 'STARTER/fonts/fontello/README.txt'
      ));

    // @todo Add .gitattributes
    //   $this->addFile()
    //   ->path($prefix . '.gitattributes')
    //   ->content(file_get_contents(
    //   $zfPath . 'STARTER/.gitattributes'
    //   ));
    // @todo Add .scss-lint.yml
    //   $this->addFile()
    //   ->path($prefix . '.scss-lint.yml')
    //   ->content(file_get_contents(
    //   $zfPath . 'STARTER/.scss-lint.yml'
    //   ));
    // @todo Add logo
    //   $this->addFile()
    //   ->path($prefix . 'logo.svg')
    //   ->content(file_get_contents(
    //   $zfPath . 'STARTER/logo.svg'
    //   ));
  }

}
